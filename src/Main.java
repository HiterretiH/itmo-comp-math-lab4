import java.io.IOException;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
        String filename = "";
        for (String arg : args) {
            if (arg.startsWith("-f=") || arg.startsWith("--file=")) {
                filename = args[0].split("=", 2)[1];
            }
            else {
                System.out.println("Неизвестный параметр: " + args[0]);
                System.out.println("Доступные параметры:");
                System.out.println("--file или -f: задать имя файла для ввода");
                System.exit(1);
            }
        }

        if (filename.isEmpty()) {
            new Application();
        }
        else {
            try {
                double[][] points = new FileInput(filename).readPoints();
                double[] x = new double[points.length];
                double[] y = new double[points.length];

                for (int i = 0; i < points.length; i++) {
                    x[i] = points[i][0];
                    y[i] = points[i][1];
                }

                new FunctionWindow(x, y);
            }
            catch (IOException | NumberFormatException e) {
                System.err.println("Ошибка ввода: " + e.getMessage());
            }
            catch (NoSuchElementException e) {
                System.err.println("Ошибка ввода: количество точек не совпадает с указанным");
            }
        }
    }
}
