import functions.Function;

public class FunctionChecker {
    public static double linearCorrelation(double[] x, double[] y) {
        double averageX = 0;
        double averageY = 0;

        for (int i = 0; i < x.length; i++) {
            averageX += x[i];
            averageY += y[i];
        }
        averageX /= x.length;
        averageY /= y.length;

        double numerator = 0;
        double sxx = 0;
        double syy = 0;

        for (int i = 0; i < x.length; i++) {
            double xDif = (x[i] - averageX);
            double yDif = (y[i] - averageY);
            numerator += xDif * yDif;
            sxx += xDif * xDif;
            syy += yDif * yDif;
        }

        return numerator / Math.sqrt(sxx * syy);
    }

    public static double standardDeviation(Function function, double[] x, double[] y) {
        double result = 0;
        for (int i = 0; i < x.length; i++) {
            double el = function.f(x[i]) - y[i];
            result += el * el;
        }

        return Math.sqrt(result / x.length);
    }
}
