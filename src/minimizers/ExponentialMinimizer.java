package minimizers;

import functions.Function;
import functions.TwoFunction;
import util.EquationBuilder;
import util.NumberFormatter;

public class ExponentialMinimizer extends AbstractMinimizer {
    public ExponentialMinimizer(double[] x, double[] y) {
        super(x, y);
    }

    @Override
    public Function minimize() {
        double[] Y = new double[n];
        for (int i = 0; i < n; i++) {
            Y[i] = Math.log(y[i]);
        }

        TwoFunction linear = (TwoFunction) new LinearMinimizer(x, Y).minimize();
        double a = Math.exp(linear.getB());
        double b = linear.getA();

        String text = new EquationBuilder()
                .append(a).append(" e").pow(NumberFormatter.format(b) + " x")
                .build();

        return new TwoFunction(t -> a * Math.exp(b * t),
                text,
                "Экспоненциальная функция",
                a, b);
    }
}
