package minimizers;

import functions.Function;
import functions.TwoFunction;
import util.EquationBuilder;

public class PowerMinimizer extends AbstractMinimizer {
    public PowerMinimizer(double[] x, double[] y) {
        super(x, y);
    }

    @Override
    public Function minimize() {
        double[] X = new double[n];
        double[] Y = new double[n];

        for (int i = 0; i < n; i++) {
            X[i] = Math.log(x[i]);
            Y[i] = Math.log(y[i]);
        }

        TwoFunction linear = (TwoFunction) new LinearMinimizer(X, Y).minimize();
        double a = Math.exp(linear.getB());
        double b = linear.getA();

        String text = new EquationBuilder()
                .append(a).append(" x").pow(b)
                .build();

        return new TwoFunction(t -> a * Math.pow(t, b),
                text,
                "Степенная функция",
                a, b);
    }
}
