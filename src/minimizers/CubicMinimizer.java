package minimizers;

import functions.FourFunction;
import functions.Function;
import solvers.FourSolver;
import util.EquationBuilder;

public class CubicMinimizer extends AbstractMinimizer {
    public CubicMinimizer(double[] x, double[] y) {
        super(x, y);
    }

    @Override
    public Function minimize() {
        double[] result = calculateCoefficients();

        String text = new EquationBuilder()
                .append(result[3]).append(" x").pow(3)
                .plus(result[2]).append(" x").pow(2)
                .plus(result[1]).append(" x")
                .plus(result[0])
                .build();

        return new FourFunction(t -> result[3] * t * t * t + result[2] * t * t + result[1] * t + result[0],
                text,
                "Кубическая функция",
                result[3], result[2], result[1], result[0]);
    }

    private double[] calculateCoefficients() {
        double sx = 0;
        double sx2 = 0;
        double sx3 = 0;
        double sx4 = 0;
        double sx5 = 0;
        double sx6 = 0;
        double sy = 0;
        double sxy = 0;
        double sx2y = 0;
        double sx3y = 0;

        for (int i = 0; i < n; i++) {
            double mx = 1;
            double my = y[i];
            sx += (mx *= x[i]);
            sx2 += (mx *= x[i]);
            sx3 += (mx *= x[i]);
            sx4 += (mx *= x[i]);
            sx5 += (mx *= x[i]);
            sx6 += mx * x[i];
            sy += my;
            sxy += (my *= x[i]);
            sx2y += (my *= x[i]);
            sx3y += my * x[i];
        }

        double[][] system = {
                {n, sx, sx2, sx3, sy},
                {sx, sx2, sx3, sx4, sxy},
                {sx2, sx3, sx4, sx5, sx2y},
                {sx3, sx4, sx5, sx6, sx3y}
        };

        return FourSolver.solve(system);
    }
}
