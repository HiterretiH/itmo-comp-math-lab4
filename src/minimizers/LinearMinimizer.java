package minimizers;

import functions.Function;
import functions.TwoFunction;
import util.EquationBuilder;

public class LinearMinimizer extends AbstractMinimizer {
    public LinearMinimizer(double[] x, double[] y) {
        super(x, y);
    }

    @Override
    public Function minimize() {
        double sx = 0;
        double sxx = 0;
        double sxy = 0;
        double sy = 0;

        for (int i = 0; i < n; i++) {
            sx += x[i];
            sxx += x[i] * x[i];
            sxy += x[i] * y[i];
            sy += y[i];
        }

        double a = (sxy * n - sx * sy) / (sxx * n - sx * sx);
        double b = (sxx * sy - sx * sxy) / (sxx * n - sx * sx);

        String text = new EquationBuilder()
                .append(a).append(" x")
                .plus(b)
                .build();

        return new TwoFunction(t -> a * t + b,
                text, "Линейная функция",
                a, b);
    }
}
