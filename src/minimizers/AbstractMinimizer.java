package minimizers;

import functions.Function;

public abstract class AbstractMinimizer {
    protected double[] x;
    protected double[] y;
    int n;

    protected AbstractMinimizer(double[] x, double[] y) {
        if (x.length != y.length || x.length == 0) {
            throw new IllegalArgumentException("x and y should have the same length");
        }

        this.x = x;
        this.y = y;
        n = x.length;
    }

    public abstract Function minimize();
}
