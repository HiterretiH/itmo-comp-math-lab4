package minimizers;

import functions.Function;
import functions.TwoFunction;
import util.EquationBuilder;

public class LogarithmicMinimizer extends AbstractMinimizer {
    public LogarithmicMinimizer(double[] x, double[] y) {
        super(x, y);
    }

    @Override
    public Function minimize() {
        double[] X = new double[n];

        for (int i = 0; i < n; i++) {
            X[i] = Math.log(x[i]);
        }

        TwoFunction linear = (TwoFunction) new LinearMinimizer(X, y).minimize();
        double a = linear.getA();
        double b = linear.getB();

        String text = new EquationBuilder()
                .append(a).append(" ln(x)")
                .plus(b)
                .build();

        return new TwoFunction(t -> a * Math.log(t) + b,
                text,
                "Логарифмическая функция",
                a, b);
    }
}
