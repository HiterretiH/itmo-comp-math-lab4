import functions.Function;
import minimizers.*;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import util.NumberFormatter;

import javax.swing.*;
import java.awt.*;

public class FunctionWindow {
    private final JFrame frame;
    private double from;
    private double to;
    private double min;
    private double max;

    public FunctionWindow(double[] x, double[] y) {
        frame = new JFrame("Функции");

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);
        layout.setAutoCreateContainerGaps(true);

        Function[] functions = {
                new LinearMinimizer(x, y).minimize(),
                new QuadraticMinimizer(x, y).minimize(),
                new CubicMinimizer(x, y).minimize(),
                new PowerMinimizer(x, y).minimize(),
                new ExponentialMinimizer(x, y).minimize(),
                new LogarithmicMinimizer(x, y).minimize(),
        };

        XYSeriesCollection dataset = new XYSeriesCollection();
        findRange(x, y);
        dataset.addSeries(getDotsSeries(x, y));
        for (Function function : functions) {
            dataset.addSeries(getSeries(function));
        }

        ChartPanel chart = generateChart(dataset);

        JLabel resultLabel = new JLabel(getInfo(functions, x, y));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(chart)
                .addComponent(resultLabel)
        );
        layout.setHorizontalGroup(layout.createParallelGroup()
                .addComponent(chart)
                .addComponent(resultLabel)
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    private String getInfo(Function[] functions, double[] x, double[] y) {
        StringBuilder result = new StringBuilder("<html>");

        double[] deviations = new double[functions.length];
        int minIndex = 0;

        for (int i = 0; i < functions.length; i++) {
            deviations[i] = FunctionChecker.standardDeviation(functions[i], x, y);
            if (deviations[i] < deviations[minIndex]) {
                minIndex = i;
            }
        }

        result.append("Лучшая функция: ")
                .append("<br>")
                .append(functions[minIndex].getTitle())
                .append(": ")
                .append(functions[minIndex].getAsText())
                .append("<br>")
                .append("\uD835\uDF39 = ")
                .append(NumberFormatter.format(deviations[minIndex]))
                .append("<br>")
                .append("<br>");

        result.append("Коэффициент линейной корреляции r = ")
                .append(NumberFormatter.format(FunctionChecker.linearCorrelation(x, y)))
                .append("<br>")
                .append("<br>");

        for (int i = 0; i < functions.length; i++) {
            result.append(functions[i].getTitle())
                    .append(": ")
                    .append(functions[i].getAsText())
                    .append("<br>")
                    .append("\uD835\uDF39 = ")
                    .append(NumberFormatter.format(deviations[i]))
                    .append("<br>");
        }

        result.append("</html>");

        return result.toString();
    }

    private void findRange(double[] x, double[] y) {
        from = x[0];
        to = x[0];
        min = y[0];
        max = y[0];

        for (int i = 0; i < x.length; i++) {
            if (x[i] < from) from = x[i];
            if (x[i] > to) to = x[i];
            if (y[i] < min) min = y[i];
            if (y[i] > max) max = y[i];
        }
    }

    private XYSeries getSeries(Function f) {
        double from = this.from - (this.to - this.from) * 0.25;
        double to = this.to + (this.to - this.from) * 0.25;

        XYSeries series = new XYSeries(f.getTitle());

        while (from <= to) {
            series.add(from, f.f(from));
            from += 0.03125;
        }

        return series;
    }

    private XYSeries getDotsSeries(double[] x, double[] y) {
        XYSeries series = new XYSeries("Точки");

        for (int i = 0; i < x.length; i++) {
            series.add(x[i], y[i]);
        }

        return series;
    }

    private ChartPanel generateChart(XYSeriesCollection dataset) {
        NumberAxis xAxis = new NumberAxis("X");
        NumberAxis yAxis = new NumberAxis("Y");

        xAxis.setRange(from, to);
        yAxis.setRange(min, max);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, true);
        renderer.setDefaultToolTipGenerator(new StandardXYToolTipGenerator());

        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
        JFreeChart chart = new JFreeChart("Функции", plot);

        int count = dataset.getSeriesCount();
        for (int i = 0; i < count; i++) {
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesLinesVisible(i, true);
        }
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(0, false);

        return getChartPanel(dataset, chart, renderer);
    }

    private static ChartPanel getChartPanel(XYSeriesCollection dataset, JFreeChart chart, XYLineAndShapeRenderer renderer) {
        ChartPanel chartPanel = new ChartPanel(chart) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(640, 480);
            }
        };

        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setMouseZoomable(true, true);

        chartPanel.addChartMouseListener(new ChartMouseListener() {
            @Override
            public void chartMouseClicked(ChartMouseEvent e) {
                ChartEntity ce = e.getEntity();
                if (ce instanceof LegendItemEntity item) {
                    int index = dataset.getSeriesIndex(item.getSeriesKey());
                    boolean visible = renderer.getSeriesLinesVisible(index);
                    if (index == 0) return;

                    renderer.setSeriesLinesVisible(index, !visible);
                }
            }

            @Override
            public void chartMouseMoved(ChartMouseEvent e) {}
        });
        return chartPanel;
    }
}
