package functions;

import java.util.function.DoubleUnaryOperator;

public class ThreeFunction implements Function {
    private final DoubleUnaryOperator f;
    private final String text;
    private final String title;
    private final double a;
    private final double b;
    private final double c;

    public ThreeFunction(DoubleUnaryOperator f, String text, String title, double a, double b, double c) {
        this.f = f;
        this.text = text;
        this.title = title;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double f(double t) {
        return f.applyAsDouble(t);
    }

    @Override
    public String getAsText() {
        return text;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
}
