package functions;

import java.util.function.DoubleUnaryOperator;

public class TwoFunction implements Function {
    private final DoubleUnaryOperator f;
    private final String text;
    private final String title;
    private final double a;
    private final double b;

    public TwoFunction(DoubleUnaryOperator f, String text, String title, double a, double b) {
        this.f = f;
        this.text = text;
        this.title = title;
        this.a = a;
        this.b = b;
    }

    @Override
    public double f(double t) {
        return f.applyAsDouble(t);
    }

    @Override
    public String getAsText() {
        return text;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }
}
