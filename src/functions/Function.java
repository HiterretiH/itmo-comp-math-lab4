package functions;

public interface Function {
    double f(double t);
    String getAsText();
    String getTitle();
}
