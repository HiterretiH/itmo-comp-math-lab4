package util;

public class EquationBuilder {
    private final StringBuilder result;

    public EquationBuilder(String string) {
        result = new StringBuilder(string);
    }

    public EquationBuilder() {
        this("");
    }

    public EquationBuilder append(String text) {
        result.append(text);
        return this;
    }

    public EquationBuilder append(double number) {
        if (number == 0) {
            return append("0");
        }
        return append(NumberFormatter.format(number));
    }

    public EquationBuilder plus(double number) {
        if (number > 0) {
            return append(" + " + NumberFormatter.format(number));

        }
        else if (number == 0) {
            return append(" + 0");
        }
        else {
            return append(" - " + NumberFormatter.format(number).substring(1));
        }
    }

    public EquationBuilder pow(String text) {
        return append("<sup>" + text + "</sup>");
    }

    public EquationBuilder pow(double number) {
        return pow(NumberFormatter.format(number));
    }

    public EquationBuilder pow(int number) {
        return pow(Integer.toString(number));
    }

    public String build() {
        return result.toString();
    }
}
