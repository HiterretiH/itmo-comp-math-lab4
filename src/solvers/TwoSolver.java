package solvers;

public class TwoSolver {
    public static double[] solve(double[][] a) {
        double[] result = new double[2];
        result[0] = (a[0][2] * a[1][1] - a[0][1] * a[1][2])/(a[0][0] * a[1][1] - a[0][1] * a[1][0]);
        result[1] = (a[0][2] * a[1][0] - a[0][0] * a[1][2])/(a[0][1] * a[1][0] - a[0][0] * a[1][1]);

        return result;
    }
}
