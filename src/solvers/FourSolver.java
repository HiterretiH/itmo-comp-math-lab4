package solvers;

public class FourSolver {
    public static double[] solve(double[][] a) {
        int size = a.length;

        for (int i = 0; i < size; i++) {
            int max = i;
            for (int j = i + 1; j < size; j++) {
                if (Math.abs(a[j][i]) > Math.abs(a[max][i])) {
                    max = j;
                }
            }
            double[] temp = a[i];
            a[i] = a[max];
            a[max] = temp;

            for (int j = i + 1; j < size; j++) {
                double factor = a[j][i] / a[i][i];
                a[j][size] -= factor * a[i][size];
                for (int k = i; k < size; k++)
                    a[j][k] -= factor * a[i][k];
            }
        }

        double[] solution = new double[size];
        for (int i = size - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < size; j++)
                sum += a[i][j] * solution[j];
            solution[i] = (a[i][size] - sum) / a[i][i];

        }

        return solution;
    }
}
