import javax.swing.*;
import java.awt.*;

public class Application {
    private final JFrame frame;

    public Application() {
        frame = new JFrame("Lab 4");

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);
        layout.setAutoCreateContainerGaps(true);

        JLabel numberLabel = new JLabel("Количество точек: ");
        JTextField numberField = new JTextField("8");
        numberField.setBorder(BorderFactory.createLineBorder(Color.black));

        JLabel pointsLabel = new JLabel("Точки для аппроксимации (через пробел): ");
        JTextArea pointsField = new JTextArea(12, 30);
        pointsField.setBorder(BorderFactory.createLineBorder(Color.black));

        JButton submitButton = new JButton("Аппроксимировать");

        submitButton.addActionListener(e -> submit(numberField.getText(), pointsField.getText()));

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup()
                                .addComponent(numberLabel)
                                .addComponent(numberField))
                        .addComponent(pointsLabel)
                        .addComponent(pointsField)
                        .addComponent(submitButton)
        );

        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(numberLabel)
                                .addComponent(numberField))
                        .addComponent(pointsLabel)
                        .addComponent(pointsField)
                        .addComponent(submitButton)
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    protected void submit(String count, String text) {
        int size;
        try {
            size = Integer.parseInt(count);
            if (size < 8 || size > 12) throw new NumberFormatException();
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Количество точек должно быть целым числом от 8 до 12", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String[] lines = text.split("\\n");

        if (lines.length < size) {
            JOptionPane.showMessageDialog(frame, "Недостаточно входных данных", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        double[] x = new double[size];
        double[] y = new double[size];

        for (int i = 0; i < size; i++) {
            String[] elements = lines[i].split("\\s", 2);
            if (elements.length < 2) {
                JOptionPane.showMessageDialog(frame, "Недостаточно чисел на строке #" + i + ": " + ": '" + lines[i] + "'", "", JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                x[i] = Double.parseDouble(elements[0].replace(',', '.'));
                y[i] = Double.parseDouble(elements[1].replace(',', '.'));
            }
            catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(frame, "Некорректный ввод точек на строке #" + i + ": '" + lines[i] + "'", "", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        new FunctionWindow(x, y);
    }
}
